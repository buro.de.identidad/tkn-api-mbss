package com.teknei.bid.service;

import java.net.Authenticator;
import java.net.InetAddress;
import java.net.PasswordAuthentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomAuthenticator extends Authenticator {
	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticator.class);
	private String usr;
	private String pwd;

	public CustomAuthenticator(String usr, String pwd) {
		this.usr = usr;
		this.pwd = pwd;
	}

	// Called when password authorization is needed
	@Override
	protected PasswordAuthentication getPasswordAuthentication() {

		// Get information about the request
		String prompt = getRequestingPrompt();
		logger.info("CustomAuthenticator prompt:" + prompt);
		String hostname = getRequestingHost();
//		hostname = "13.59.84.19";
		logger.info("CustomAuthenticator hostname:" + hostname);
		InetAddress ipaddr = getRequestingSite();
//		hostname = "/13.59.84.19";
		logger.info("CustomAuthenticator ipaddr:" + ipaddr);
		String username = usr;
		logger.info("CustomAuthenticator usr:" + usr);
		String password = pwd;
		logger.info("CustomAuthenticator pwd:" + pwd);

		// Return the information (a data holder that is used by Authenticator)
		return new PasswordAuthentication(username, password.toCharArray());

	}
}
