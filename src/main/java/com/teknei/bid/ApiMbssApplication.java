package com.teknei.bid;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableDiscoveryClient
public class ApiMbssApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiMbssApplication.class, args);
    }

    @Bean
    public Docket api() {
        /*
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.teknei.bid.dto.rest"))
                .paths(PathSelectors.ant("/mbss/*"))
                .build()
                .apiInfo(apiInfo());
                */
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                //.apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.teknei.bid.controller.rest"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
    	//log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".apiInfo ");
        ApiInfo apiInfo = new ApiInfo(
                "TKN REST API FOR MBSS SERVICES",
                "TKN API reference for mbss soap service.",
                "1.0.0",
                "Terms of service",
                //"jamaro@teknei.com",
                new springfox.documentation.service.Contact("Jorge Amaro", "", "jamaro@teknei.com"),
                "License of API",
                "API license URL");
        return apiInfo;
    }
}
