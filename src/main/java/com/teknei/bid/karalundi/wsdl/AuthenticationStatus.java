
package com.teknei.bid.karalundi.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para authenticationStatus.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="authenticationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SUCCESS"/>
 *     &lt;enumeration value="FAIL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "authenticationStatus")
@XmlEnum
public enum AuthenticationStatus {

    SUCCESS,
    FAIL;

    public String value() {
        return name();
    }

    public static AuthenticationStatus fromValue(String v) {
        return valueOf(v);
    }

}
