package com.teknei.bid.controller.rest.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFingerService;
import com.teknei.bid.dto.Fingers;

@Service
public class FingerController {

	private static Logger logger = LoggerFactory.getLogger(FingerController.class);
	@Autowired
	private KaralundiFingerService karalundiFingerervice;	
	@Value("${tkn.karalundi.resource.finger.configuration1}")
    private String configuration;
	@Value("${tkn.karalundi.resource.finger.enterpriseId}")
    private String enterpriseId;
	@Value("${tkn.karalundi.resource.finger.speakerListClient}")
    private String speakerListClient;
	
	// KARALUNDI Creacion de HASH y duplicado de huellas en faltates 
	private  String tempHuella;
	public String getTempHuella() {
		return tempHuella;
	}
	
	/**
	 * Enroll fingers
	 * @param id
	 * @param fingers
	 * @return OK or BAD_REQUEST or UNPROCESSABLE_ENTITY if fail.
	 */
	public ResponseEntity<OperationResult> sendEnrollMF(String id, Fingers fingers) {
		RequestKaralundi requestParam = new RequestKaralundi();
		OperationResult operationResult = new OperationResult();
		try {
			requestParam = startSessionAndGetFingerMap(fingers, id);	
			requestParam.setSpeaker(id);
			requestParam.setMapFiles(completFakeFingers(requestParam.getMapFiles(),id));			
			
			for (String key :requestParam.getMapFiles().keySet()) {				
				requestParam.setFile(requestParam.getMapFiles().get(key));
				requestParam.setData(getDataChanelFromFinger(key));
				ResponseKaralundi respKara = karalundiFingerervice.enrollMF(requestParam);	
				logger.info("Status: " + respKara.getStatus());
				if (respKara.getStatus() != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
					logger.info("Usuario: " + id + " Enrolamiento exitoso de la huella");					
				} else {
					logger.error("Ocurrió un error al enrolar la huella usuario:" + id);
					logger.info("ErrorMsg:" + respKara.getErrorMsg());
					operationResult.setResultOK(false);
					operationResult.setErrorMessage("20001");
					//TODO DISENRROLL
					karalundiFingerervice.deleteSegments(requestParam);
					return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
				}
			}
			operationResult.setResultOK(true);
			operationResult.setMessage("Se han almacenado las huellas en el expediente");
			//TODO ADDTOLIST
			logger.error(""+karalundiFingerervice.addSpeakersToList(requestParam));
			return new ResponseEntity<>(operationResult, HttpStatus.OK);
		}catch (Exception e) {
			logger.error("Ocurrió un error al parsear el json", e);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			//TODO DISENRROLL
			if (!e.getMessage().contains("Duplicated biometric")) {
				karalundiFingerervice.deleteSegments(requestParam);
//				karalundiFingerervice.disenroll(requestParam);
			}
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		}finally {
			if(!requestParam.getSession().isEmpty()) {
        		karalundiFingerervice.endSession(requestParam);
            }
		}
	}
	
	/**
	 * Metodo para completar mapa fingers en caso de ser menor a 8.
	 * @param inputFingers
	 * @return  Map<String, File> 
	 */
	private Map<String, File> completFakeFingers(Map<String, File> inputFingers,String idClie) {
		if (inputFingers.size() < 10) {
			try {
				// Creando un mapa completo dummy..
				Map<String, File> dummyFingers = new HashMap<String, File>();
				dummyFingers.put("left-thumb",inputFingers.get("left-thumb")!=null?inputFingers.get("left-thumb"):getFileFromByte("left-thumb", tempHuella,idClie));
				dummyFingers.put("left-index",inputFingers.get("left-index")!=null?inputFingers.get("left-index"):getFileFromByte("left-index", tempHuella,idClie));
				dummyFingers.put("left-middle",inputFingers.get("left-middle")!=null?inputFingers.get("left-middle"):getFileFromByte("left-middle", tempHuella,idClie));
				dummyFingers.put("left-ring",inputFingers.get("left-ring")!=null?inputFingers.get("left-ring"):getFileFromByte("left-ring", tempHuella,idClie));
				dummyFingers.put("left-little",inputFingers.get("left-little")!=null?inputFingers.get("left-little"):getFileFromByte("left-little", tempHuella,idClie));
				dummyFingers.put("right-thumb",inputFingers.get("right-thumb")!=null?inputFingers.get("right-thumb"):getFileFromByte("right-thumb", tempHuella,idClie));
				dummyFingers.put("right-index",inputFingers.get("right-index")!=null?inputFingers.get("right-index"):getFileFromByte("right-index", tempHuella,idClie));
				dummyFingers.put("right-middle",inputFingers.get("right-middle")!=null?inputFingers.get("right-middle"):getFileFromByte("right-middle", tempHuella,idClie));
				dummyFingers.put("right-ring",inputFingers.get("right-ring")!=null?inputFingers.get("right-ring"):getFileFromByte("right-ring", tempHuella,idClie));
				dummyFingers.put("right-little",inputFingers.get("right-little")!=null?inputFingers.get("right-little"):getFileFromByte("right-little", tempHuella,idClie));				
				return dummyFingers;
			} catch (IOException e) {
				logger.error("ERROR: No se pudo obtener finger base: ", e.getMessage());
				return inputFingers;
			}
		}
		return inputFingers;
	}
	
	/**
	 * Método para identificar la exixtencia de una persona en una lista.  Necesita una huella activa.
	 * @param jsonRequest
	 * @return FOUND, NOT_FOUND or BAD_REQUEST if fail.
	 */
	public ResponseEntity<OperationResult> sendIdentifyMf(String id, Fingers fingers) {
		logger.info(" INFO: " + this.getClass().getName()+".sendIdentifyMf:");
		OperationResult operationResult = new OperationResult();
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			String operationId = id;
			logger.info(" OperationId: " + operationId);	
			requestParam = startSessionAndGetFingerMap(fingers,id);
			requestParam.setSpeaker(speakerListClient);
			logger.info(" Buscando en : " + speakerListClient);	
			ResponseKaralundi respKara = karalundiFingerervice.identifyMF(requestParam);	
			logger.info("Status: " + respKara.getStatus()); // FAIL, SUCCESS
			return result(respKara, operationId);

		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		}finally {
        	if(!requestParam.getSession().isEmpty()) {
        		karalundiFingerervice.endSession(requestParam);
            }
        }
	}
	
	/**
	 * Metodo 1 a 1 para verificar la identidad de una persona con registro.  Necesita una huella activa.
	 * @param jsonRequest
	 * @return OK, NOT_FOUND. UNPROCESSABLE_ENTITY or BAD_REQUEST  if fail.
	 */
	public ResponseEntity<OperationResult> sendVerifyMF(String id, Fingers fingers) {
		logger.info(" INFO: " + this.getClass().getName()+".sendVerifyMF:");
		OperationResult operationResult = new OperationResult();
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			String operationId = id;
			logger.info(" OperationId: " + operationId);
						
			requestParam = startSessionAndGetFingerMap(fingers,id);
			requestParam.setSpeaker(operationId);
			
			
			ResponseKaralundi respKara = karalundiFingerervice.verifyMF(requestParam);	
			logger.info("Status: " + respKara.getStatus()); // FAIL, SUCCESS

			return result(respKara, operationId);

		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		}finally {
        	if(!requestParam.getSession().isEmpty()) {
        		karalundiFingerervice.endSession(requestParam);
            }
        }
	}
	
	/**
	 * TODO metodo provicional en caso de falla en SendVerifyMF
	 * @param id
	 * @param speakerList
	 * @return FOUND, NOT_FOUND. BAD_REQUEST  if fail.
	 */
	public HttpStatus provisionalSendIdentifyMF(Fingers fingers, List<String> speakerList) {
		logger.info(" INFO: " + this.getClass().getName()+".provisionalSendVerifyMF:");
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			requestParam = startSessionAndGetFingerMap(fingers,"");
			for(String operationId: speakerList) {
				requestParam.setSpeaker(operationId);
				ResponseKaralundi respKara = karalundiFingerervice.verifyMF(requestParam);	
				if (respKara.getStatus() != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
					if ("ACCEPTED".equals(respKara.getOutcome())){
						return  HttpStatus.FOUND;
					}
				}
			}
			return  HttpStatus.NOT_FOUND;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			return  HttpStatus.BAD_REQUEST;
		}finally {
        	if(!requestParam.getSession().isEmpty()) {
        		karalundiFingerervice.endSession(requestParam);
            }
        }	
	}
	
	/**
	 * Metodo que inicia session y mapea fingers
	 * @param fingers
	 * @return
	 * @throws IOException
	 */
	private RequestKaralundi startSessionAndGetFingerMap(Fingers fingers,String idClie) throws IOException {
		RequestKaralundi requestParam = new RequestKaralundi();
		ResponseKaralundi respKara = karalundiFingerervice.startSession(configuration, enterpriseId, "");
		requestParam.setSession(respKara.getSession());
		requestParam.setMapFiles(mapFromRequestFinger(fingers, idClie));	
		return requestParam;
	}	
	
	/**
	 * Genera el campo Data para las peticiones enroll karalundi
	 * @param finger
	 * @return
	 */
	private String getDataChanelFromFinger(String finger){		
			switch (finger) {
			case "left-thumb":	return "{\"fpg\":6}";
			case "left-index":	return "{\"fpg\":7}";
			case "left-middle":	return "{\"fpg\":8}";
			case "left-ring":	return "{\"fpg\":9}";
			case "left-little":	return "{\"fpg\":10}";
			case "right-thumb":	return "{\"fpg\":1}";
			case "right-index": return "{\"fpg\":2}";			
			case "right-middle":return "{\"fpg\":3}";
			case "right-ring":  return "{\"fpg\":4}";
			case "right-little":return "{\"fpg\":5}";
			}
			return finger;
	}

	/**
	 * Metodo para convertir String to file
	 * @param key
	 * @param encodedImg
	 * @return
	 * @throws IOException
	 */
	private File getFileFromByte(String key, String encodedImg,String idClie) throws IOException {	
		if( encodedImg == null || encodedImg.isEmpty()) {
			return null ;
		}
		File convFile = null;
		byte[] decodeImg = Base64Utils.decodeFromString(encodedImg);
//		byte[] decodeImg = Base64.getDecoder().decode(encodedImg);
		convFile = new File("/home/"+idClie+"_"+key + ".wsq");		
		FileUtils.writeByteArrayToFile(convFile, decodeImg);		
		return convFile;
	}
	
	/**
	 * Metodo para crear mapa de files
	 * @param fingers
	 * @return
	 * @throws IOException
	 */	
	private Map<String,File> mapFromRequestFinger(Fingers fingers,String idClie) throws IOException{
		Map<String,File> wsqFiles = new HashMap<String, File>();
		wsqFiles.put("left-thumb"  ,getFileFromByte("left-thumb"  , fingers.getLt(), idClie));
		tempHuella = fingers.getLt() != null ? fingers.getLt() : null ;
		wsqFiles.put("left-index"  ,getFileFromByte("left-index"  , fingers.getLi(), idClie));
		tempHuella = fingers.getLi() != null ? fingers.getLi() : tempHuella ;
		wsqFiles.put("left-middle" ,getFileFromByte("left-middle" , fingers.getLm(), idClie));
		tempHuella = fingers.getLm() != null ? fingers.getLm() : tempHuella ;
		wsqFiles.put("left-ring"   ,getFileFromByte("left-ring"   , fingers.getLr(), idClie));
		tempHuella = fingers.getLr() != null ? fingers.getLr() : tempHuella ;
		wsqFiles.put("left-little" ,getFileFromByte("left-little" , fingers.getLl(), idClie));	
		tempHuella = fingers.getLl() != null ? fingers.getLl() : tempHuella ;		
		wsqFiles.put("right-thumb" ,getFileFromByte("right-thumb" , fingers.getRt(), idClie));
		tempHuella = fingers.getRt() != null ? fingers.getRt() : tempHuella ;
		wsqFiles.put("right-index" ,getFileFromByte("right-index" , fingers.getRi(), idClie));
		tempHuella = fingers.getRi() != null ? fingers.getRi() : tempHuella ;
		wsqFiles.put("right-middle",getFileFromByte("right-middle", fingers.getRm(), idClie));
		tempHuella = fingers.getRm() != null ? fingers.getRm() : tempHuella ;
		wsqFiles.put("right-ring"  ,getFileFromByte("right-ring"  , fingers.getRr(), idClie));
		tempHuella = fingers.getRr() != null ? fingers.getRr() : tempHuella ;
		wsqFiles.put("right-little",getFileFromByte("right-little", fingers.getRl(), idClie));
		tempHuella = fingers.getRl() != null ? fingers.getRl() : tempHuella ;	
		wsqFiles.values().removeAll(Collections.singleton(null));	
		logger.info("INFO: " + wsqFiles.size() +" Huellas Recibidas.");	
		return wsqFiles;	
	}	
	
	/**
	 * Manejador de resultado
	 * @param respKara
	 * @param operationId
	 * @return FOUND, NOT_FOUND or UNPROCESSABLE_ENTITY if fail.
	 */
	private ResponseEntity<OperationResult> result(ResponseKaralundi respKara, String operationId){
		OperationResult operationResult = new OperationResult();
		if (respKara.getStatus() != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {			
			if ("ACCEPTED".equals(respKara.getOutcome())){
				logger.info("Usuario:" + operationId + "Verificación exitosa usuario:" + operationId);
				operationResult.setResultOK(true);
				return new ResponseEntity<>(operationResult, HttpStatus.FOUND);
			}else {
				logger.error("Verificación rechazada" + "usuario:" + operationId);
				operationResult.setResultOK(false);
				operationResult.setErrorMessage("20001");
				return new ResponseEntity<>(operationResult, HttpStatus.NOT_FOUND);
			}			
		} else {
			logger.error("Ocurrió un error al verificar" + "usuario:" + operationId);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
		}		
	}
	
	
	
	public void enrollmentAutomatic() {
		
		// ubicaciones
		String lista = "D:\\cargaKaralundi\\idminucias.json";
		String carpeta = "D:\\cargaKaralundi\\wsqFiles\\";
		String extencion = ".wsq";
		// configuraciones Karalundi
		String configuration = "CONFIG_FINGER_10";
		String enterpriseId = "5bd0917354a78";
		
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			//obteniendo lista de ids
			FileReader paramsJson = new FileReader(lista);		
			JsonObject data = (JsonObject) new JsonParser().parse(paramsJson);				
			JsonArray ids = data.getAsJsonArray("ids");
			//enrrollando lista de ids
			for(JsonElement op: ids) {				
				String id = op.toString();
				id = id.replace("\"", "");
				logger.info("*************** ENROLLANDO: {} ***************",id);				
				//obteniendo huella y creando mapa
				File wsq = new File(carpeta+id+extencion);	
				Map<String, File> wsqFiles = new HashMap<String, File>();
				wsqFiles.put("left-thumb", wsq);
				wsqFiles.put("left-index", wsq);
				wsqFiles.put("left-middle", wsq);
				wsqFiles.put("left-ring", wsq);
				wsqFiles.put("left-little", wsq);
				wsqFiles.put("right-thumb", wsq);
				wsqFiles.put("right-index", wsq);
				wsqFiles.put("right-middle", wsq);
				wsqFiles.put("right-ring", wsq);
				wsqFiles.put("right-little", wsq);
				requestParam.setMapFiles(wsqFiles);

				// Iniciando session..
				ResponseKaralundi respKaralundiSession = karalundiFingerervice.startSession(configuration,	enterpriseId, "");
				if (respKaralundiSession.getSession().isEmpty()) {
					throw new Exception(
							"No se pudo iniciar session en Karalundi ErrorMsg:" + respKaralundiSession.getErrorMsg());
				}
				
				// Creando request..
				requestParam.setSession(respKaralundiSession.getSession());
				requestParam.setSpeaker(id);
				
				// Enrrollando 10 Segmentos..
				for (String key : requestParam.getMapFiles().keySet()) {
					requestParam.setFile(requestParam.getMapFiles().get(key));
					requestParam.setData(getDataChanelFromFinger(key));
					ResponseKaralundi respKaralundiEnroll = karalundiFingerervice.enrollMF(requestParam);
					logger.info("Status: " + respKaralundiEnroll.getStatus());
					if (respKaralundiEnroll.getStatus() != null
							&& "SUCCESS".equals(respKaralundiEnroll.getStatus().toUpperCase())) {
						logger.info("Enrolamiento exitoso de la huella "+ key);
					} else {
						logger.error("Ocurrió un error al enrolar la huella usuario:" + enterpriseId);
						logger.info("ErrorMsg:" + respKaralundiEnroll.getErrorMsg());
						karalundiFingerervice.disenroll(requestParam);
						throw new Exception("ErrorMsg:" + respKaralundiEnroll.getErrorMsg());
					}
				}
				
				// Se completo el enrrolamiento...
				logger.info("############################# Se completo el enrrolamiento con exito.");
				String respAddList = karalundiFingerervice.addSpeakersToList(requestParam);
				logger.info("############################# Speaker Enrolado Correctamente : {} Agregado a la lista {}", id, respAddList);
				
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {			
			e.printStackTrace();
		} finally {
			if (requestParam.getSession() != null && !requestParam.getSession().isEmpty()) {
				karalundiFingerervice.endSession(requestParam);
			}
		}
	}
	
}
