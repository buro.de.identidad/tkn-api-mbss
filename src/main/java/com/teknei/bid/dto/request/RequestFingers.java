package com.teknei.bid.dto.request;

import com.teknei.bid.dto.Fingers;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class RequestFingers implements Serializable {

    private String id;
    private Fingers fingers;
    private String imageType;

}
