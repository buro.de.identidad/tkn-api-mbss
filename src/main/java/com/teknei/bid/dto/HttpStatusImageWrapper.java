package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Created by Amaro on 18/07/2017.
 */
@Data
@AllArgsConstructor
public class HttpStatusImageWrapper implements Serializable{

    private HttpStatus httpStatus;
    private String id;
    private String score;

}