package com.teknei.bid.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleResponse implements Serializable {

    private String id;
    private String status;
    private String time;

}
