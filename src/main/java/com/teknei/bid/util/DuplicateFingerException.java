package com.teknei.bid.util;

import org.json.JSONArray;

public class DuplicateFingerException extends Exception {

    private String message;
    private JSONArray validatedSequence;

    public DuplicateFingerException(String message){
        super(message);
        this.message = message;
    }

    public DuplicateFingerException(String message, JSONArray validatedSequence){
        super(message);
        this.message = message;
        this.validatedSequence = validatedSequence;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JSONArray getValidatedSequence() {
        return validatedSequence;
    }

    public void setValidatedSequence(JSONArray validatedSequence) {
        this.validatedSequence = validatedSequence;
    }
}