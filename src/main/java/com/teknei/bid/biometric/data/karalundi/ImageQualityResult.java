package com.teknei.bid.biometric.data.karalundi;

public class ImageQualityResult {

	private String outcome;
	private Integer age;
	private Object gender;
	private Integer smile;
	private Object glasses;
	private Double eyesConfidence;

	/**
	 * @return the outcome
	 */
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @param outcome the outcome to set
	 */
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * @return the gender
	 */
	public Object getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Object gender) {
		this.gender = gender;
	}

	/**
	 * @return the smile
	 */
	public Integer getSmile() {
		return smile;
	}

	/**
	 * @param smile the smile to set
	 */
	public void setSmile(Integer smile) {
		this.smile = smile;
	}

	/**
	 * @return the glasses
	 */
	public Object getGlasses() {
		return glasses;
	}

	/**
	 * @param glasses the glasses to set
	 */
	public void setGlasses(Object glasses) {
		this.glasses = glasses;
	}

	/**
	 * @return the eyesConfidence
	 */
	public Double getEyesConfidence() {
		return eyesConfidence;
	}

	/**
	 * @param eyesConfidence the eyesConfidence to set
	 */
	public void setEyesConfidence(Double eyesConfidence) {
		this.eyesConfidence = eyesConfidence;
	}
}
