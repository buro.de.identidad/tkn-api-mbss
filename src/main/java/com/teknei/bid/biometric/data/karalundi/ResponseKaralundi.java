package com.teknei.bid.biometric.data.karalundi;

import com.teknei.bid.biometric.data.ResponseBiometric;

public class ResponseKaralundi extends ResponseBiometric {

	// Campo utilizado para start-session
	private String session;

	// Campo utilizado para is-trained
	private String trained;

	private String txId;
	private String outcome;
	private Integer netSpeech;
	private Integer saturation;
	private Integer speechRatio;
	private Integer snr;
	private Object qualityOutcome;
	private ImageQualityResult imageQualityResult;
	private Object fingerprintQualityResult;

	// Campos para enroll
	private Object bvpValidationOutcome;
	private Integer confidence;
	private Integer bvpValidationScore;
	private Object qualityOutcomeDetail;

	// Campos agregados para Verify
	private Integer sidScore;
	private Integer ivrScore;
	private Integer mobileLQLSScore;
	private Integer mobileMQLSScore;
	private Object antispoofingOutcome;
	private String speakerOutcome;
	private String verificationOutcome;
	private Object documentprocessingResult;

	// Campos agregados para identify
	private String sidId;
	private Object speakerResults;

	/**
	 * @return the session
	 */
	public String getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(String session) {
		this.session = session;
	}

	/**
	 * @return the trained
	 */
	public String getTrained() {
		return trained;
	}

	/**
	 * @param trained the trained to set
	 */
	public void setTrained(String trained) {
		this.trained = trained;
	}

	/**
	 * @return the txId
	 */
	public String getTxId() {
		return txId;
	}

	/**
	 * @param txId the txId to set
	 */
	public void setTxId(String txId) {
		this.txId = txId;
	}

	/**
	 * @return the outcome
	 */
	public String getOutcome() {
		return outcome;
	}

	/**
	 * @param outcome the outcome to set
	 */
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	/**
	 * @return the netSpeech
	 */
	public Integer getNetSpeech() {
		return netSpeech;
	}

	/**
	 * @param netSpeech the netSpeech to set
	 */
	public void setNetSpeech(Integer netSpeech) {
		this.netSpeech = netSpeech;
	}

	/**
	 * @return the saturation
	 */
	public Integer getSaturation() {
		return saturation;
	}

	/**
	 * @param saturation the saturation to set
	 */
	public void setSaturation(Integer saturation) {
		this.saturation = saturation;
	}

	/**
	 * @return the speechRatio
	 */
	public Integer getSpeechRatio() {
		return speechRatio;
	}

	/**
	 * @param speechRatio the speechRatio to set
	 */
	public void setSpeechRatio(Integer speechRatio) {
		this.speechRatio = speechRatio;
	}

	/**
	 * @return the snr
	 */
	public Integer getSnr() {
		return snr;
	}

	/**
	 * @param snr the snr to set
	 */
	public void setSnr(Integer snr) {
		this.snr = snr;
	}

	/**
	 * @return the qualityOutcome
	 */
	public Object getQualityOutcome() {
		return qualityOutcome;
	}

	/**
	 * @param qualityOutcome the qualityOutcome to set
	 */
	public void setQualityOutcome(Object qualityOutcome) {
		this.qualityOutcome = qualityOutcome;
	}

	/**
	 * @return the imageQualityResult
	 */
	public ImageQualityResult getImageQualityResult() {
		return imageQualityResult;
	}

	/**
	 * @param imageQualityResult the imageQualityResult to set
	 */
	public void setImageQualityResult(ImageQualityResult imageQualityResult) {
		this.imageQualityResult = imageQualityResult;
	}

	/**
	 * @return the fingerprintQualityResult
	 */
	public Object getFingerprintQualityResult() {
		return fingerprintQualityResult;
	}

	/**
	 * @param fingerprintQualityResult the fingerprintQualityResult to set
	 */
	public void setFingerprintQualityResult(Object fingerprintQualityResult) {
		this.fingerprintQualityResult = fingerprintQualityResult;
	}

	/**
	 * @return the bvpValidationOutcome
	 */
	public Object getBvpValidationOutcome() {
		return bvpValidationOutcome;
	}

	/**
	 * @param bvpValidationOutcome the bvpValidationOutcome to set
	 */
	public void setBvpValidationOutcome(Object bvpValidationOutcome) {
		this.bvpValidationOutcome = bvpValidationOutcome;
	}

	/**
	 * @return the confidence
	 */
	public Integer getConfidence() {
		return confidence;
	}

	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(Integer confidence) {
		this.confidence = confidence;
	}

	/**
	 * @return the bvpValidationScore
	 */
	public Integer getBvpValidationScore() {
		return bvpValidationScore;
	}

	/**
	 * @param bvpValidationScore the bvpValidationScore to set
	 */
	public void setBvpValidationScore(Integer bvpValidationScore) {
		this.bvpValidationScore = bvpValidationScore;
	}

	/**
	 * @return the qualityOutcomeDetail
	 */
	public Object getQualityOutcomeDetail() {
		return qualityOutcomeDetail;
	}

	/**
	 * @param qualityOutcomeDetail the qualityOutcomeDetail to set
	 */
	public void setQualityOutcomeDetail(Object qualityOutcomeDetail) {
		this.qualityOutcomeDetail = qualityOutcomeDetail;
	}

	/**
	 * @return the sidScore
	 */
	public Integer getSidScore() {
		return sidScore;
	}

	/**
	 * @param sidScore the sidScore to set
	 */
	public void setSidScore(Integer sidScore) {
		this.sidScore = sidScore;
	}

	public void setSidScore(Double sidScore) {
		this.sidScore = sidScore.intValue();
	}
	/**
	 * @return the ivrScore
	 */
	public Integer getIvrScore() {
		return ivrScore;
	}

	/**
	 * @param ivrScore the ivrScore to set
	 */
	public void setIvrScore(Integer ivrScore) {
		this.ivrScore = ivrScore;
	}

	/**
	 * @return the mobileLQLSScore
	 */
	public Integer getMobileLQLSScore() {
		return mobileLQLSScore;
	}

	/**
	 * @param mobileLQLSScore the mobileLQLSScore to set
	 */
	public void setMobileLQLSScore(Integer mobileLQLSScore) {
		this.mobileLQLSScore = mobileLQLSScore;
	}

	/**
	 * @return the mobileMQLSScore
	 */
	public Integer getMobileMQLSScore() {
		return mobileMQLSScore;
	}

	/**
	 * @param mobileMQLSScore the mobileMQLSScore to set
	 */
	public void setMobileMQLSScore(Integer mobileMQLSScore) {
		this.mobileMQLSScore = mobileMQLSScore;
	}

	/**
	 * @return the antispoofingOutcome
	 */
	public Object getAntispoofingOutcome() {
		return antispoofingOutcome;
	}

	/**
	 * @param antispoofingOutcome the antispoofingOutcome to set
	 */
	public void setAntispoofingOutcome(Object antispoofingOutcome) {
		this.antispoofingOutcome = antispoofingOutcome;
	}

	/**
	 * @return the speakerOutcome
	 */
	public String getSpeakerOutcome() {
		return speakerOutcome;
	}

	/**
	 * @param speakerOutcome the speakerOutcome to set
	 */
	public void setSpeakerOutcome(String speakerOutcome) {
		this.speakerOutcome = speakerOutcome;
	}

	/**
	 * @return the verificationOutcome
	 */
	public String getVerificationOutcome() {
		return verificationOutcome;
	}

	/**
	 * @param verificationOutcome the verificationOutcome to set
	 */
	public void setVerificationOutcome(String verificationOutcome) {
		this.verificationOutcome = verificationOutcome;
	}

	/**
	 * @return the documentprocessingResult
	 */
	public Object getDocumentprocessingResult() {
		return documentprocessingResult;
	}

	/**
	 * @param documentprocessingResult the documentprocessingResult to set
	 */
	public void setDocumentprocessingResult(Object documentprocessingResult) {
		this.documentprocessingResult = documentprocessingResult;
	}

	/**
	 * @return the sidId
	 */
	public String getSidId() {
		return sidId;
	}

	/**
	 * @param sidId the sidId to set
	 */
	public void setSidId(String sidId) {
		this.sidId = sidId;
	}

	/**
	 * @return the speakerResults
	 */
	public Object getSpeakerResults() {
		return speakerResults;
	}

	/**
	 * @param speakerResults the speakerResults to set
	 */
	public void setSpeakerResults(Object speakerResults) {
		this.speakerResults = speakerResults;
	}

}