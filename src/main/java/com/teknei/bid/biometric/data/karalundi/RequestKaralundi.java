package com.teknei.bid.biometric.data.karalundi;

import java.io.File;
import java.util.List;
import java.util.Map;

public class RequestKaralundi {

	private String session;
	private String speaker;
	private String param;
	private File file;
	private String list;
	private String channel;
	private String image;
	//private Map<String, Object> data;
	private String data;

	// Campos utilizados para Fingers
	private String leftIndex;
	private String leftMiddle;
	private String leftRing;
	private String leftLittle;
	private String leftThumb;
	private String rightIndex;
	private String rightMiddle;
	private String rightRing;
	private String rightLittle;
	private String rightThumb;
	
	private Map<String,File> listFiles;

	
	public RequestKaralundi() {}

	/**
	 * 
	 * @param session
	 * @param speaker
	 * @param param
	 * @param file
	 */
	public RequestKaralundi(String session, String speaker, String param, File file) {
		this.session = session;
		this.speaker = speaker;
		this.param = param;
		this.file = file;
	}
	
	public RequestKaralundi(String session, String speaker, String param, File file, String list, String channel) {
		this.session = session;
		this.speaker = speaker;
		this.param = param;
		this.file = file;
		this.list = list;
		this.channel = channel;
	}

	/**
	 * @return the session
	 */
	public String getSession() {
		return session;
	}

	/**
	 * @param session the session to set
	 */
	public void setSession(String session) {
		this.session = session;
	}

	/**
	 * @return the speaker
	 */
	public String getSpeaker() {
		return speaker;
	}

	/**
	 * @param speaker the speaker to set
	 */
	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	/**
	 * @return the param
	 */
	public String getParam() {
		return param;
	}

	/**
	 * @param param the param to set
	 */
	public void setParam(String param) {
		this.param = param;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	
	/**
	 * @return the list
	 */
	public String getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(String list) {
		this.list = list;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}


	/**
	 * @return the leftIndex
	 */
	public String getLeftIndex() {
		return leftIndex;
	}

	/**
	 * @param leftIndex the leftIndex to set
	 */
	public void setLeftIndex(String leftIndex) {
		this.leftIndex = leftIndex;
	}

	/**
	 * @return the leftMiddle
	 */
	public String getLeftMiddle() {
		return leftMiddle;
	}

	/**
	 * @param leftMiddle the leftMiddle to set
	 */
	public void setLeftMiddle(String leftMiddle) {
		this.leftMiddle = leftMiddle;
	}

	/**
	 * @return the leftRing
	 */
	public String getLeftRing() {
		return leftRing;
	}

	/**
	 * @param leftRing the leftRing to set
	 */
	public void setLeftRing(String leftRing) {
		this.leftRing = leftRing;
	}

	/**
	 * @return the leftLittle
	 */
	public String getLeftLittle() {
		return leftLittle;
	}

	/**
	 * @param leftLittle the leftLittle to set
	 */
	public void setLeftLittle(String leftLittle) {
		this.leftLittle = leftLittle;
	}

	/**
	 * @return the leftThumb
	 */
	public String getLeftThumb() {
		return leftThumb;
	}

	/**
	 * @param leftThumb the leftThumb to set
	 */
	public void setLeftThumb(String leftThumb) {
		this.leftThumb = leftThumb;
	}

	/**
	 * @return the rightIndex
	 */
	public String getRightIndex() {
		return rightIndex;
	}

	/**
	 * @param rightIndex the rightIndex to set
	 */
	public void setRightIndex(String rightIndex) {
		this.rightIndex = rightIndex;
	}

	/**
	 * @return the rightMiddle
	 */
	public String getRightMiddle() {
		return rightMiddle;
	}

	/**
	 * @param rightMiddle the rightMiddle to set
	 */
	public void setRightMiddle(String rightMiddle) {
		this.rightMiddle = rightMiddle;
	}

	/**
	 * @return the rightRing
	 */
	public String getRightRing() {
		return rightRing;
	}

	/**
	 * @param rightRing the rightRing to set
	 */
	public void setRightRing(String rightRing) {
		this.rightRing = rightRing;
	}

	/**
	 * @return the rightLittle
	 */
	public String getRightLittle() {
		return rightLittle;
	}

	/**
	 * @param rightLittle the rightLittle to set
	 */
	public void setRightLittle(String rightLittle) {
		this.rightLittle = rightLittle;
	}

	/**
	 * @return the rightThumb
	 */
	public String getRightThumb() {
		return rightThumb;
	}

	/**
	 * @param rightThumb the rightThumb to set
	 */
	public void setRightThumb(String rightThumb) {
		this.rightThumb = rightThumb;
	}

	public Map<String,File> getMapFiles() {
		return listFiles;
	}

	public void setMapFiles(Map<String,File> files) {
		this.listFiles = files;
	}
}
