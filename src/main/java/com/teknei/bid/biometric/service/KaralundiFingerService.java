package com.teknei.bid.biometric.service;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;

public interface KaralundiFingerService {

	/**
	 * Inicio de sesion para uso de servicios karalundi
	 * @param configuration
	 * @param enterpriseId
	 * @param device
	 * @return
	 */
	ResponseKaralundi startSession(String configuration, String enterpriseId, String device);
	/**
	 * Fin de session en karalundi
	 * @param request
	 * @return
	 */
	ResponseKaralundi endSession(RequestKaralundi request);	
	ResponseKaralundi isTrained(RequestKaralundi request);
	
	/**
	 * Guardado de minucias en Karalundi
	 * @param request
	 * @return
	 */
	ResponseKaralundi enrollMF(RequestKaralundi request);
	ResponseKaralundi disenroll(RequestKaralundi request);
	ResponseKaralundi deleteSegments(RequestKaralundi request);
	
	/**
	 * Busca un usuario mediante el id y las huellas.
	 * @param request
	 * @return
	 */
	ResponseKaralundi verifyMF(RequestKaralundi request);
	
	/**
	 * Busca un usuario mediante las huellas.
	 * Es necesario la lista de koala
	 * @param request
	 * @return
	 */
	ResponseKaralundi identifyMF(RequestKaralundi request);	
	ResponseKaralundi updateMF(RequestKaralundi request);
	
	/**
	 * Agrega al speacker a una lista.
	 * @param request
	 * @param userType
	 * @return
	 */
	String addSpeakersToList(RequestKaralundi request);
	String addSpeakersToListIneValidation(RequestKaralundi request,String speakerList);
	
}
