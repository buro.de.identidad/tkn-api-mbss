package com.teknei.bid.biometric.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.teknei.bid.biometric.service.InvokerService;

@Service
public class InvokerServiceImpl implements InvokerService{

	private static Logger logger = LoggerFactory.getLogger(InvokerServiceImpl.class); 

	
	public String send(String endpoint, Map<String, Object> params) {
		logger.info("INFO:"+this.getClass().getName()+".send");
		String response = null;
    	try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
					public void checkClientTrusted(X509Certificate[] certs, String authType) {}
					public void checkServerTrusted(X509Certificate[] certs, String authType) {}
				}
			};
			
			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
            URL url = new URL(endpoint);
            logger.info("INFO endpoint:"+endpoint);

    		StringBuilder postData = new StringBuilder();
    		for (Map.Entry<String,Object> param : params.entrySet()) {
    			if (postData.length() != 0) postData.append('&');
    			postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
    			postData.append('=');
    			postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
    		}
    		
    		byte[] postDataBytes = postData.toString().getBytes("UTF-8");

    		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    		
    		conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
    		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
    		conn.setDoOutput(true);
    		conn.getOutputStream().write(postDataBytes);
    		
    		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    		StringBuilder data = new StringBuilder();
    		for (int c; (c = in.read()) >= 0;) {
    			data.append((char)c);
    		}
    		
    		response = data.toString();
            logger.info("INFO response:"+response);
    	}catch(Exception ex) {
    		logger.error("Ocurrió un error al invocar el servicio externo", ex);
    	}
    	
    	return response;
	}

	@Override
	public String sendMultipart(String endpoint, Map<String, Object> params) {
		String response = null;
    	
    	try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
					public void checkClientTrusted(X509Certificate[] certs, String authType) {}
					public void checkServerTrusted(X509Certificate[] certs, String authType) {}
				}
			};
			
			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			
            URL url = new URL(endpoint);

    		StringBuilder postData = new StringBuilder();
    		for (Map.Entry<String,Object> param : params.entrySet()) {
    			if (postData.length() != 0) postData.append('&');
    			postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
    			postData.append('=');
    			postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
    		}
    		
    		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
    		
    		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    		
    		conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            //conn.setRequestProperty("Content-Type", "multipart/form-data");

    		conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=*****");
    		//conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
    		conn.setDoOutput(true);
    		conn.getOutputStream().write(postDataBytes);
    		
    		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
    		StringBuilder data = new StringBuilder();
    		for (int c; (c = in.read()) >= 0;) {
    			data.append((char)c);
    		}
    		
    		response = data.toString();
    		
    		//if(response != null)
    		//	sessionStatus = new Gson().fromJson(response, SessionStatus.class);
    		
    	}catch(Exception ex) {
    		logger.error("Ocurrió un error al invocar el servicio externo", ex);
    	}
    	
    	return response;
	}

}
