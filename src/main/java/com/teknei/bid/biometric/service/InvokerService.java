package com.teknei.bid.biometric.service;

import java.util.Map;

public interface InvokerService {

    public String send(String resource, Map<String,Object> params);
    
	public String sendMultipart(String endpoint, Map<String, Object> params);

}
