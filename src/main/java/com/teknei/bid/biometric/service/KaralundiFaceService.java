package com.teknei.bid.biometric.service;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;

public interface KaralundiFaceService {

	ResponseKaralundi startSession(String configuration, String enterpriseId, String device);
	ResponseKaralundi enrollMF(RequestKaralundi params);
	ResponseKaralundi verifyMF(RequestKaralundi paramsRequest);
	
	ResponseKaralundi isTrained(RequestKaralundi paramsRequest);
	ResponseKaralundi endSession(RequestKaralundi paramsRequest);
	ResponseKaralundi deleteSegments(RequestKaralundi paramsRequest);
	
	ResponseKaralundi identifyMF(RequestKaralundi paramsRequest);
	
}
