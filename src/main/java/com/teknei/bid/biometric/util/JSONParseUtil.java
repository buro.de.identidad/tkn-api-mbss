package com.teknei.bid.biometric.util;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;

public class JSONParseUtil {

	public static ResponseKaralundi stringToResponseKaralundi(String value) {
		if(value == null)
			return null;
		value = catchNumberFormat(value);
		return new Gson().fromJson(value, ResponseKaralundi.class);
	}
	
	private static String catchNumberFormat(String value) {
		if(value.contains("sidScore")) {
			try {
				JSONObject bulid = new JSONObject(value);
				Double sidScore =  bulid.optDouble("sidScore", 0.0);
				if(sidScore!=null) {
					bulid.put("sidScore", sidScore.intValue());
				}
				return bulid.toString();
			} catch (JSONException e) {
				
			}
		}
		return value;		
	}
	
}
