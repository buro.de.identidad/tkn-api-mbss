//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
//
package com.morpho.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.morpho.mbss.generic.wsdl.*; 

import java.util.UUID;

public class WSHelper 
{
	private static final Logger log = LoggerFactory.getLogger(WSHelper.class);
    public WSHelper() {
    }

    public static Request countPersonReq() {
    	//log.info("lblancas:: [tkn-api-mbss] :: WSHelper.countPersonReq ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        CountPersonRequest countPersonRequest = new CountPersonRequest();
        countPersonRequest.setBasicRequest(basic);
        Request request = new Request();
        request.setCountPersonRequest(countPersonRequest);
        request.setRequestType(RequestType.COUNT_PERSON);
        return request;
    }

    public static Request getPersonInfoListReq() {
    	//log.info("lblancas:: [tkn-api-mbss] :: WSHelper.getPersonInfoListReq ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        GetPersonInfoListRequest request = new GetPersonInfoListRequest();
        request.setBasicRequest(basic);
        Request r = new Request();
        r.setGetPersonInfoListRequest(request);
        r.setRequestType(RequestType.GET_PERSON_INFO_LIST);
        return r;
    }

    public static Image imageFromByte(byte[] data, int imageHeight, int imageWidth, ImageFormatType imageType) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.imageFromByte(byte[] data, int imageHeight, int imageWidth, ImageFormatType imageType)  ");
        Image image = new Image();
        image.setBuffer(data);
        image.setFormat(imageType);
        if (imageHeight != 0 && imageWidth != 0) {
            image.setHeight(imageHeight);
            image.setWidth(imageWidth);
        }

        return image;
    }

    public static Image imageFromByte(byte[] data, ImageFormatType imageType) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.imageFromByte(byte[] data, ImageFormatType imageType) ");
        return imageFromByte(data, 0, 0, imageType);
    }

    public static Tenprint buildTenPrint() {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.Tenprint ");
        Tenprint tp = new Tenprint();
        return tp;
    }

    public static Person buildPersonByHand(String pin, byte[] splatLeft, byte[] splatRigth, byte[] splatThumbs, FingerprintSampleType sampleType) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.buildPersonByHand ");
        FingerprintSample sample = new FingerprintSample();
        sample.setSampleType(sampleType);
        Slap442 slap442 = new Slap442();
        if (splatLeft != null) {
            slap442.setLeftHand(imageFromByte(splatLeft, ImageFormatType.WSQ));
        }

        if (splatRigth != null) {
            slap442.setRightHand(imageFromByte(splatRigth, ImageFormatType.WSQ));
        }

        if (splatThumbs != null) {
            slap442.setThumbs(imageFromByte(splatThumbs, ImageFormatType.WSQ));
        }

        sample.setSlap442(slap442);
        Registration registration = new Registration();
        registration.getFingerprintSample().add(sample);
        Person person = new Person();
        person.setId(pin);
        person.getRegistration().add(registration);
        return person;
    }

    public static Person buildPersonFromFingers(String pin, byte[] rThumb, byte[] rIndex, byte[] rMiddle, byte[] rRing, byte[] rLittle, byte[] lThumb, byte[] lIndex, byte[] lMiddle, byte[] lRing, byte[] lLittle, ImageFormatType imageType, FingerprintSampleType sampleType) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.buildPersonFromFingers ");
        Tenprint tp = new Tenprint();
        if (rThumb != null) {
            tp.setRightThumb(imageFromByte(rThumb, imageType));
        }

        if (rIndex != null) {
            tp.setRightIndex(imageFromByte(rIndex, imageType));
        }

        if (rMiddle != null) {
            tp.setRightMiddle(imageFromByte(rMiddle, imageType));
        }

        if (rRing != null) {
            tp.setRightRing(imageFromByte(rRing, imageType));
        }

        if (rLittle != null) {
            tp.setRightLittle(imageFromByte(rLittle, imageType));
        }

        if (lThumb != null) {
            tp.setLeftThumb(imageFromByte(lThumb, imageType));
        }

        if (lIndex != null) {
            tp.setLeftIndex(imageFromByte(lIndex, imageType));
        }

        if (lMiddle != null) {
            tp.setLeftMiddle(imageFromByte(lMiddle, imageType));
        }

        if (lRing != null) {
            tp.setLeftRing(imageFromByte(lRing, imageType));
        }

        if (lLittle != null) {
            tp.setLeftLittle(imageFromByte(lLittle, imageType));
        }

        FingerprintSample sample = new FingerprintSample();
        sample.setTenprint(tp);
        sample.setSampleType(sampleType);
        Registration registration = new Registration();
        registration.getFingerprintSample().add(sample);
        Person person = new Person();
        person.setId(pin);
        person.getRegistration().add(registration);
        return person;
    }

    public static Person buildPersonFromPortrait(String pin, byte[] portraitData, ImageFormatType imageType, FaceSampleType sampleType) {
        log.info("lblancas:: [tkn-api-mbss] ::WSHelper.buildPersonFromPortrait  ");
        FaceSample sample = new FaceSample();
        StillImage stillImage = new StillImage();
        stillImage.setImage(imageFromByte(portraitData, imageType));
        sample.setStillImage(stillImage);
        sample.setSampleType(sampleType);
        Registration registration = new Registration();
        registration.getFaceSample().add(sample);
        Person person = new Person();
        person.setId(pin);
        person.getRegistration().add(registration);
        return person;
    }

    public static Request getPersonFromIdRequest(String id) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.getPersonFromIdRequest(String id)  ::"+id);
        BasicRequest basic = new BasicRequest();
        basic.setId(id);
        GetPersonRequest personRequest = new GetPersonRequest();
        personRequest.setId(id);
        personRequest.setBasicRequest(basic);
        Request req = new Request();
        req.setRequestType(RequestType.GET_PERSON);
        req.setGetPersonRequest(personRequest);
        return req;
    }

    public static Request codePersonReq(Person person) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.codePersonReq(Person person) ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString() + person.getId());
        EncodePersonRequest codePersonRequest = new EncodePersonRequest();
        codePersonRequest.setBasicRequest(basic);
        codePersonRequest.setPerson(person);
        Request request = new Request();
        request.setEncodePersonRequest(codePersonRequest);
        request.setRequestType(RequestType.ENCODE_PERSON);
        return request;
    }

    public static Request codePersonReq(Person person, boolean segementOnly) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.codePersonReq(Person person, boolean segementOnly) ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        EncodePersonRequest codePersonRequest = new EncodePersonRequest();
        codePersonRequest.setBasicRequest(basic);
        codePersonRequest.setPerson(person);
        if(segementOnly) {
            codePersonRequest.setEncodeLevel(EncodeLevel.WITH_SEQUENCE_CHECK); //Funciona para huellas duplicadas OK
            //codePersonRequest.setEncodeLevel(EncodeLevel.SEGMENT_ONLY);
        }
        Request request = new Request();
        request.setEncodePersonRequest(codePersonRequest);
        request.setRequestType(RequestType.ENCODE_PERSON);
        return request;
    }

    public static Request insertPersonReq(Person person) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.insertPersonReq ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        InsertPersonRequest insertPersonRequest = new InsertPersonRequest();
        insertPersonRequest.setBasicRequest(basic);
        insertPersonRequest.setPerson(person);
        Request request = new Request();
        request.setInsertPersonRequest(insertPersonRequest);
        request.setRequestType(RequestType.INSERT_PERSON);
        return request;
    }

    public static Request updatePersonReq(Person person) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.updatePersonReq ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        UpdatePersonRequest updatePersonRequest = new UpdatePersonRequest();
        updatePersonRequest.setBasicRequest(basic);
        updatePersonRequest.setPerson(person);
        Request request = new Request();
        request.setUpdatePersonRequest(updatePersonRequest);
        request.setRequestType(RequestType.UPDATE_PERSON);
        return request;
    }

    public static Request deletePersonReq(String pin) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.deletePersonReq(String pin) pin:"+pin);
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        DeletePersonRequest deletePersonRequest = new DeletePersonRequest();
        deletePersonRequest.setBasicRequest(basic);
        deletePersonRequest.setId(pin);
        Request request = new Request();
        request.setDeletePersonRequest(deletePersonRequest);
        request.setRequestType(RequestType.DELETE_PERSON);
        return request;
    }

    public static Request searchPPReq(Person person) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.searchPPReq(Person person) ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        PersonToPersonMatchRequest ppMatchingRequest = new PersonToPersonMatchRequest();
        ppMatchingRequest.setBasicRequest(basic);
        ppMatchingRequest.setPerson(person);
        ppMatchingRequest.setUsePositionInMatching(false);
        Request request = new Request();
        request.setPersonToPersonMatchRequest(ppMatchingRequest);
        request.setRequestType(RequestType.MATCH_PERSON_TO_PERSON);
        return request;
    }

    public static Request verifReq(Person person, String pin) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.verifReq(Person person, String pin)  ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        AuthenticatePersonRequest authenticatePersonRequest = new AuthenticatePersonRequest();
        authenticatePersonRequest.setBasicRequest(basic);
        authenticatePersonRequest.setPerson(person);
        authenticatePersonRequest.getReferenceId().add(pin);
        Request request = new Request();
        request.setAuthenticatePersonRequest(authenticatePersonRequest);
        request.setRequestType(RequestType.AUTHENTICATE_PERSON);
        return request;
    }

    public static Request verifReq(Person personSearch, Person personRef) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.verifReq(Person personSearch, Person personRef) ");
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        PersonToPersonAuthenticateRequest ppAuthenticateRequest = new PersonToPersonAuthenticateRequest();
        ppAuthenticateRequest.setBasicRequest(basic);
        ppAuthenticateRequest.setPersonSearch(personSearch);
        ppAuthenticateRequest.getPersonRef().add(personRef);
        Request request = new Request();
        request.setPersonToPersonAuthRequest(ppAuthenticateRequest);
        request.setRequestType(RequestType.AUTHENTICATE_PERSON_TO_PERSON);
        return request;
    }

    public static Request deleteReq(String id) {
        //log.info("lblancas:: [tkn-api-mbss] :: WSHelper.deleteReq  id:"+id);
        BasicRequest basic = new BasicRequest();
        basic.setId(UUID.randomUUID().toString());
        DeletePersonRequest deletePersonRequest = new DeletePersonRequest();
        deletePersonRequest.setBasicRequest(basic);
        deletePersonRequest.setId(id);
        Request request = new Request();
        request.setDeletePersonRequest(deletePersonRequest);
        request.setRequestType(RequestType.DELETE_PERSON);
        return request;
    }
}
